# Doodlez: Hand-drawn Doodle Classification App

Web app to classify doodles using a Convolutional Recurrent Neural Network.

Includes:

## Python notebook used for training

Model training is done in a Python notebook running on the Colab environment.

## Google Cloud Function for a Serverless Computing backend

Data preprocessing and model inference is handled by a deployed Google Cloud Function triggered by
an HTTP request.

## Web client

A React app allows the user to draw on the screen and requests a prediction based on the drawing.
Model version is controlled via environment variables. This way, deploying and or rolling back is as
easy as updating environment variables and deploying the new build. The appropriate model will then be referenced when the request is being served by the Google Cloud Function.

## Live app

[Doodlez](https://doodlez-1229a.web.app/)
