import React from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap';
import CanvasDraw from "react-canvas-draw";
import { GridLoader } from 'react-spinners';
import styles from './canvas.module.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faUndo } from '@fortawesome/free-solid-svg-icons';


interface IProps{
    canvasWidth:number;
    canvasHeight:number;
    brushRadius: number;
}
interface IState{
    strokes: number,
    prompt: string,
    points: any[],
    isLoading: boolean,
}


export default class Canvas extends React.Component <IProps, IState>{
    state: IState = {
        strokes: 0,
        prompt: "Start Drawing",
        points: [],
        isLoading: false,

    }
    canvas!: any | null;

    async queryModel(){
        // Simple POST request with a JSON body using fetch
        let modelPath = "projects/" + process.env.REACT_APP_PROJECT_ID;
        modelPath += "/models/" + process.env.REACT_APP_MODEL_ID;
        modelPath += "/versions/" + process.env.REACT_APP_VERSION_NAME;

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "points": this.state.points,
                "width": this.canvas != null ? this.canvas.canvas.interface.width:0,
                "height": this.canvas != null ? this.canvas.canvas.interface.height:0,
                "model_path": modelPath,
            })
        };
        const response = await fetch(process.env.REACT_APP_GOOGLE_CLOUD_FUNCTION_URL || '', requestOptions)
            .then(response => {
                return response.json();
            });
        
        this.setState({
            prompt: response["prediction"],
            isLoading: false
        })
    }

    addStroke(){
        if(this.canvas != null){
            const canvasData: any = JSON.parse(this.canvas.getSaveData())
            const linesArray = canvasData.lines;
            if (linesArray[this.state.strokes] === undefined){
                return;
            }
            let newLinePoints = linesArray[this.state.strokes].points;
            newLinePoints = newLinePoints.slice(1,newLinePoints.length-1)
            this.setState({
                strokes: this.state.strokes + 1,
                points: this.state.points.concat([newLinePoints]),
                isLoading: true,
                prompt: "Thinking"
            }, () => {
                this.queryModel()
            })
        }
    }
    clearCanvas(){
        this.setState({
            strokes: 0,
            prompt: "Start Drawing",
            points: [],
            isLoading: false,
        })
    }
    undoStroke(){
        let newStrokes = this.state.strokes-1;
        newStrokes = newStrokes < 0 ? 0 : newStrokes;

        let newPrompt = newStrokes === 0 ? "Start Drawing" : this.state.prompt;
        const newPoints = newStrokes === 0 ? [] : this.state.points.slice(0, newStrokes)
        this.setState({
            strokes: newStrokes,
            prompt: newPrompt,
            points: newPoints,
            isLoading: false
        })
    }
    render(){
        let {prompt, isLoading} = this.state;
        return (
            <Container className={styles.container}>
                <Row className={styles.loadingSpinner}>
                    <GridLoader loading={isLoading} color={"#F5A623"}/>
                </Row>
                <Row>
                    <h3
                        className={styles.canvas}
                        >{prompt.charAt(0).toUpperCase() + prompt.substring(1)}</h3>
                </Row>
                <Row>    
                    <CanvasDraw
                        style={{backgroundColor: "#454545"}}
                        brushRadius={this.props.brushRadius}
                        brushColor={"#000"}
                        lazyRadius={0}
                        canvasWidth={this.props.canvasWidth}
                        canvasHeight={this.props.canvasHeight}
                        ref={canvasDraw => (this.canvas = canvasDraw)}
                        className={styles.canvas}
                        onChange={()=>{
                            if(this.canvas != null){
                                this.addStroke();
                            }
                            
                    }}/>
                </Row>
                
            
                <Row className={styles.buttonRow}>
                    <Col>
                        <Button variant="warning"
                            onClick={()=>{
                                this.undoStroke();
                                if(this.canvas != null){
                                    this.canvas.undo();
                                }
                            }}>
                                <FontAwesomeIcon icon={faUndo} title="Undo"/>
                                </Button>
                                
                    </Col>
                    <Col>
                        <Button variant="warning"
                        onClick={()=>{
                            this.clearCanvas();
                            if(this.canvas != null){
                                this.canvas.clear();
                            }
                        }}>
                            <FontAwesomeIcon icon={faTrash} title="Clear"/>

                        </Button>
                    </Col>
                </Row>
            </Container>
        )
    }

}