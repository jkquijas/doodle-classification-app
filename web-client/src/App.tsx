import React from 'react';
import { Col, Row } from 'react-bootstrap';
import './App.css';
import styles from './app.module.scss';

import Canvas from './canvas'
import logo from './images/Skeleton.gif'

function App() {
  return (
    <div className="App" style={{backgroundColor: "#454545"}}>
        <div className={styles.headerDiv}>
          <Row noGutters>
          <Col md={1} lg={1} className="d-none d-lg-block">
            <img src={logo} alt={'Logo'}/>
          </Col>
          <Col xs={12} sm={12} md={10} lg={10}>
          <h1 className={styles.header}>Doodlez Sketch Classifier</h1>
          <h5 className={styles.header}>Draw on the grid and a neural network will guess what you're drawing!</h5>

          </Col>
          </Row>
        </div>
        <div className={styles.canvasDiv}>
          <Canvas
            canvasWidth={350}
            canvasHeight={350}
            brushRadius={3}
            >
          </Canvas>
        </div>
     
    </div>
  );
}

export default App;
