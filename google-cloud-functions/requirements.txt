Flask==1.1.2
numpy==1.19.4
simplification==0.5.1
requests==2.25.1
google-api-python-client
functions-framework==2.1.0