from class_names import get_predicted_class_name
from preprocessing import preprocess_raw_points


import os
import json
from google.api_core.client_options import ClientOptions
import googleapiclient.discovery

# For local development and debugging, use service account's private key JSON file
# Otherwise, deploy with service account directly as CLI option
# e.g., `gcloud functions deploy ... --service-account service-account-email`
if os.getenv('GOOGLE_APPLICATION_CREDENTIALS_FILE'):
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.getenv('GOOGLE_APPLICATION_CREDENTIALS_FILE')

endpoint = 'https://us-central1-ml.googleapis.com'
client_options = ClientOptions(api_endpoint=endpoint)
ml_resource = googleapiclient.discovery.build('ml', 'v1', client_options=client_options).projects()


def classify_doodle(request):
    # Set CORS headers for the preflight request
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': os.getenv("WEB_CLIENT_URL"),
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }
        return ('', 204, headers)

    # Set CORS headers for main requests
    headers = {
        'Access-Control-Allow-Origin': os.getenv("WEB_CLIENT_URL"),
        'Access-Control-Allow-Credentials': 'true'
    }

    # Preprocess raw data
    points_lists = request.json["points"]
    instance = preprocess_raw_points(points_lists)
    input_data_json = {
        "signature_name": "serving_default",
        "instances": instance
    }

    # Query model
    model_path = request.json["model_path"]
    request = ml_resource.predict(name=model_path, body=input_data_json)
    response = request.execute()

    if 'error' in response:
        return (json.dumps({"prediction": 'Keep Drawing'}), 200, headers)

    predicted_class = get_predicted_class_name(response['predictions'][0])
    return (json.dumps({"prediction": predicted_class}), 200, headers)
