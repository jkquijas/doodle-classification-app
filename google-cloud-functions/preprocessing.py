import numpy as np
from simplification.cutil import simplify_coords


def preprocess_raw_points(points_lists):
    '''
        Preprocess a list of lists of dictionaries of the form: {x:float, y:float}
        and return a matrix with 3 columns:
        x and y deltas, and a binary indicator for stroke end
    '''


    # Store as n X 3 array
    stroke_end_indexes, points_arr = [], []
    for point_list in points_lists:
        preprocessed_points = [
            [point['x'], point['y']]
            for point in point_list
        ]
        stroke_end_indexes.append(len(preprocessed_points)-1)
        points_arr.append(preprocessed_points)

    points_arr = np.concatenate(points_arr).astype(float)

    # Map stroke indexes into the n X 3 matrix
    stroke_end_indexes = np.cumsum(stroke_end_indexes)
    stroke_end_indexes += np.array([i for i in range(len(points_lists))])

    # Normalize coordinates to 0-255
    min_vals = np.min(points_arr, axis=0)
    max_vals = np.max(points_arr, axis=0)
    scale = max_vals - min_vals
    points_arr[:, 0:2] -= min_vals
    points_arr[:, 0:2] /= scale
    points_arr[:, 0:2] *= 255

    # Simplify piecewise linear curves using Ramer–Douglas–Peucker algorithm
    epsilon = 1.5
    split_strokes = np.split(points_arr[:, 0:2], stroke_end_indexes+1)[:-1]
    simplified_strokes = [
        simplify_coords(split_stroke, epsilon)
        for split_stroke in split_strokes
    ]
    # Recompute stroke end indexes of simplified curves
    simplified_stroke_end_indexes = np.cumsum([len(simp)-1 for simp in simplified_strokes])
    simplified_stroke_end_indexes += np.array([i for i in range(len(split_strokes))])

    # Store as n X 3 array and normalize to 0-1
    simplified_strokes = np.concatenate(simplified_strokes)/255
    simplified_stroke_ends = np.zeros(len(simplified_strokes))
    simplified_stroke_ends[simplified_stroke_end_indexes] = 1

    # Compute deltas
    deltas = simplified_strokes[1:, 0:2] - simplified_strokes[0:-1, 0:2]
    deltas = np.c_[deltas, simplified_stroke_ends[1:]].astype(np.float16)

    instance = [deltas.tolist()]

    return instance